import torch
import torch.nn as nn
import torch.nn.functional as F
import math

"""
Тут делаем свою сеть ResNet
"""

class Bottleneck(nn.Module):
    expansion = 4

    def __init__(self, inplanes, planes, stride=1, downsample=None):
        super(Bottleneck, self).__init__()
        
        self.conv = nn.Sequential(
            # in dim reduction 1x1
            nn.Conv2d(inplanes, planes, kernel_size=1, bias=False),
            nn.BatchNorm2d(planes),
            nn.ReLU(inplace=True),
            
            # main conv block
            nn.Conv2d(planes, planes, kernel_size=3, stride=stride, padding=1, bias=False),
            nn.BatchNorm2d(planes),
            nn.ReLU(inplace=True),
            
            # output block
            nn.Conv2d(planes, planes * self.expansion, kernel_size=1, bias=False),
            nn.BatchNorm2d(planes * self.expansion)
        )
        
        # downsample block 
        self.downsample = downsample
        self.relu  = nn.ReLU(inplace=True)

    def forward(self, x):
        residual = x

        out = self.conv(x)
        
        if self.downsample is not None:
            residual = self.downsample(x)

        out += residual
        
        out = self.relu(out)
        
        return out
    
class ResNet(nn.Module):

    def __init__(self, block, layers, num_classes=100):
        
        super(ResNet, self).__init__()
        self.inplanes = 16
        
        self.net = nn.Sequential(
            nn.Conv2d(3, self.inplanes, kernel_size=6, stride=2, padding=2),
            nn.BatchNorm2d(self.inplanes),
            nn.ReLU(inplace=True),
            
            self._make_layer(block, self.inplanes, layers[0]),
            self._make_layer(block, self.inplanes, layers[1], stride=2),
            self._make_layer(block, self.inplanes, layers[2], stride=2),
            
            nn.AvgPool2d(4)
        )
        
        self.classifier = nn.Sequential(
            nn.Linear(self.inplanes, num_classes),
            nn.BatchNorm2d(num_classes),
            nn.ReLU(inplace=True),
            nn.Linear(num_classes, num_classes)
        )
        
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()
            elif isinstance(m, nn.Sequential):
                for n in m.modules():
                    if isinstance(n, nn.Conv2d):
                        k = n.kernel_size[0] * n.kernel_size[1] * n.out_channels
                        n.weight.data.normal_(0, math.sqrt(2. / k))
                    elif isinstance(n, nn.BatchNorm2d):
                        n.weight.data.fill_(1)
                        n.bias.data.zero_()
                    

    def _make_layer(self, block, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                nn.Conv2d(self.inplanes, planes * block.expansion, kernel_size=1, stride=stride),
                nn.BatchNorm2d(planes * block.expansion)
            )
        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample))
        self.inplanes = planes * block.expansion
        for _ in range(1, blocks):
            layers.append(block(self.inplanes, planes))

        return nn.Sequential(*layers)

    def forward(self, x):
        out = self.net(x)
        out = out.view(-1, self.inplanes)
        out = self.classifier(out)
        return out
    
def Net():
    return ResNet(Bottleneck, [9,9,6], 100)
