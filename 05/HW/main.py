import torch
import torch.nn as nn
from   torch.autograd import Variable
from   torch.optim import SGD

class WordDataSet:

    def __init__(self, word):
        self.chars2idx = {}
        self.indexs = []
        for c in word:
            if c not in self.chars2idx:
                self.chars2idx[c] = len(self.chars2idx)

            self.indexs.append(self.chars2idx[c])

        self.vec_size = len(self.chars2idx)
        self.seq_len = len(word)

    def get_one_hot(self, idx):
        x = torch.zeros(self.vec_size)
        x[idx] = 1
        return x

    def __iter__(self):
        return zip(self.indexs[:-1], self.indexs[1:])

    def __len__(self):
        return self.seq_len

    def get_char_by_id(self, id):
        for c, i in self.chars2idx.items():
            if id == i: return c
        return None


class Embedding(nn.Module):
    def __init__(self, in_dims, emb_dims):
        super(Embedding, self).__init__()

        self.W  = nn.Parameter( torch.randn(emb_dims, in_dims) )

    def forward(self, x):
        result = torch.matmul( self.W, x.t() )
        return result


class LSTM_RNN(nn.Module):
    def __init__(self, in_size, out_size, inner_size):
        super(LSTM_RNN, self).__init__()

        self.in_size    = in_size
        self.out_size   = out_size

        self.height     = inner_size

        self.emb        = Embedding( in_size, inner_size )

        self.inr_W      = nn.Parameter( torch.randn( 4 * self.height, 2 * self.height ) )
        self.out_W      = nn.Parameter( torch.randn( self.out_size, self.height ) )

        self.prev       = { 'c': Variable( torch.zeros( self.height, 1 ) ).cuda(),
                            'h': Variable( torch.zeros( self.height, 1 ) ).cuda() }

    def clear(self):
        self.prev = { 'c': Variable( torch.zeros( self.height, 1 ) ).cuda(),
                      'h': Variable( torch.zeros( self.height, 1 ) ).cuda() }

    def forward(self, x):
        in_v        = Variable( torch.zeros( 2 * self.height, 1 ) ).cuda()

        in_v[:self.height]    = self.prev['h']
        in_v[self.height:]    = self.emb( x )

        output  = torch.matmul( self.inr_W, in_v )

        ig      = torch.sigmoid( output[0 : self.height] )
        fg      = torch.sigmoid( output[self.height : 2 * self.height] )
        og      = torch.sigmoid( output[2 * self.height: 3 * self.height] )
        gg      = torch.sigmoid( output[3 * self.height:] )

        self.prev['c'] = fg * self.prev['c'] + ig * gg
        self.prev['h'] = og * torch.tanh( self.prev['c'] )

        output = torch.matmul( self.out_W, self.prev['h'] )

        return output.t_()

word        = 'abcdefghijklmnopqrstuvwxyz'
ds          = WordDataSet(word=word)

emb_dim     = 10

rnn         = LSTM_RNN(in_size=ds.vec_size, out_size=ds.vec_size, inner_size=emb_dim).cuda()

criterion   = nn.CrossEntropyLoss()
e_cnt       = 200
optim       = SGD(rnn.parameters(), lr=0.5)

for epoch in range(e_cnt):
    rnn.clear()
    loss = 0
    optim.zero_grad()
    for sample, next_sample in ds:
        x = Variable(ds.get_one_hot(sample).unsqueeze(0)).cuda()
        target = Variable(torch.LongTensor([next_sample])).cuda()

        y = rnn(x)

        loss += criterion(y, target)

    if epoch % 25 == 0:
        print("epoch = {:3} : loss {:.3f}".format(epoch, loss.item()))
    loss.backward()
    optim.step()

rnn.clear()
rnn.eval()

id          = 0
softmax     = nn.Softmax(dim=1)
predword    = ds.get_char_by_id(id)
for c in enumerate(word[:-1]):
    x   = Variable(ds.get_one_hot(id).unsqueeze(0)).cuda()
    y   = rnn(x)
    y   = softmax(y)
    m, id = torch.max(y, 1)
    id = id.data[0]
    predword += ds.get_char_by_id(id)
print ('Prediction: ' , predword)

"""
Результат:

epoch =   0 : loss 90.247
epoch =  25 : loss 5.995
epoch =  50 : loss 1.788
epoch =  75 : loss 1.054
epoch = 100 : loss 0.744
epoch = 125 : loss 0.574
epoch = 150 : loss 0.463
epoch = 175 : loss 0.388
Prediction:  abcdefghijklmnopqrstuvwxyz

"""