import torch
import os
import torch.nn                 as nn
import torchvision.transforms   as transforms
import torch.optim              as optim

from torch.utils.data           import DataLoader
from torchvision.datasets       import cifar
from cifar_model                import Net
from torch.autograd             import Variable
from tensorboardX               import SummaryWriter
from optparse                   import OptionParser
from tqdm                       import *

parser = OptionParser("Train cifar10 neural network")

parser.add_option("-i", "--input", dest="input", default='./',
                  help="Cifar data root directory") 

parser.add_option('-m',"--model", dest="model", default='./model_save/final.model',
                  help="Model base path ")

parser.add_option('-c',"--checkpoint", dest="checkpoint",
                  help="Check point for load model and continue train") 

parser.add_option('-e',"--epoch", dest="epoch", default='10', type=int,
                  help="Count epoch") 

parser.add_option("-l", "--log", dest="log", type='string', default='./log',
                  help="Path to tensorboard log") 

def adjust_learning_rate(optimizer, epoch, base_lr):
    lr = base_lr * ( 0.5 ** (epoch // 5) )
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr
    return  lr

def train(options):
    base_lr = 0.001 
    classes = ('plane', 'car', 'bird', 'cat',
               'deer', 'dog', 'frog', 'horse', 'ship', 'truck')

    writer = SummaryWriter(log_dir=options.log)
    transform = transforms.Compose(
        [   
            transforms.ToTensor(),
            transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010))
    ])

    transform_test = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010))
    ])

    print("Loading data....")
    trainset = cifar.CIFAR10(options.input, train=True, transform=transform, download=True)

    trainloader = DataLoader(trainset, batch_size=16,
                                              shuffle=True, num_workers=2)

    testset = cifar.CIFAR10(options.input, train=False, transform=transform_test)
    testloader = DataLoader(testset, batch_size=16,
                                             shuffle=False, num_workers=2)

    print("Creating model...")
    net = Net().cuda()

    criterion = nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(net.parameters(), lr=base_lr)
    
    start_from_epoch = 0
    if options.checkpoint is not None and os.path.exists(options.checkpoint):
        cp_dic = torch.load(options.checkpoint)
        net.load_state_dict(cp_dic['net'])
        optimizer.load_state_dict(cp_dic['optimizer'])
        start_from_epoch = cp_dic['epoch']

    print("Start train....")
    for epoch in range(start_from_epoch, options.epoch):
        train_loss = 0.0

        epoch_lr = adjust_learning_rate(optimizer, epoch, base_lr)

        print ('Train epoch: ', epoch)
        net.train(True)
        for i, data in tqdm(enumerate(trainloader, 0), total=len(trainloader)):
            inputs, labels = data

            inputs, labels = Variable(inputs).cuda(), Variable(labels).cuda()

            optimizer.zero_grad()

            outputs = net(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()

            train_loss += loss.data[0]
            writer.add_scalar('loss/iter_train',loss.data[0], epoch * len(trainloader) + i )

        train_loss /= len(trainloader)

        net.eval()
        test_loss = 0.0
        class_correct = list(0. for i in range(10))
        class_total = list(0. for i in range(10))
        print('Test epoch: ', epoch)
        for i, data in tqdm(enumerate(testloader, 0), total=len(testloader)):
            inputs, labels = data

            inputs, labels = Variable(inputs, volatile=True).cuda(), Variable(labels, volatile=True).cuda()
            outputs = net(inputs)

            loss = criterion(outputs, labels)
            test_loss += loss.data[0]
            _, predicted = torch.max(outputs.data, 1)
            c = (predicted == labels.data).squeeze()
            for i in range(outputs.size(0)):
                label = labels.data[i]
                class_correct[label] += c[i]
                class_total[label] += 1


        test_loss /= len(testloader)
        accuracy= {}
        avg_accuracy = 0
        for i in range(10):
            accuracy[classes[i]] = 100 * class_correct[i] / class_total[i]
            avg_accuracy += accuracy[classes[i]]

        writer.add_scalars('loss/avg_epoch_error', {'train':train_loss, 'test':test_loss}, epoch )
        writer.add_scalars('loss/class_accuracy', accuracy , epoch)
        writer.add_scalar('loss/avg_accuracy', avg_accuracy/10, epoch)

        writer.add_scalar('loss/epoch_lr', epoch_lr, epoch)

        if epoch %2 ==0:
            torch.save({
                    'epoch': epoch + 1,
                    'net': net.state_dict(),
                    'optimizer': optimizer.state_dict()
                }, options.model + '_chekpoint_%03d.pth'%epoch )

    torch.save({
        'net': net.state_dict()
        }, options.model + '.pth')

if __name__ == '__main__':
    (options, args) = parser.parse_args()
    if not os.path.exists( options.log ):
        os.mkdir(options.log)
    train(options)
