import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

# копипаст из лекции из примера MNIST
class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        
        self.net = nn.Sequential(nn.BatchNorm2d(3))
        
        # вход 32x32, выход 32x32 (32-9+4*2)/1 + 1 = 32
        self.net.add_module('conv3_32', nn.Conv2d(3, 32, kernel_size=9, padding=4))
        
        # добавляем дропаут и бн
        self.net.add_module('bn_1', nn.BatchNorm2d(32))
        self.net.add_module('do_1', nn.Dropout(0.5))
            
        self.net.add_module('relu_1', nn.ReLU())
        
        # конволюция, которая уменьшает размер картинки в два раза 
        # (32-2)/2+1=16
        self.net.add_module('convhalf1', nn.Conv2d(32, 32, kernel_size=2, stride=2)) 
        
        # вход 16x16, выход 16x16 (16-5+2x2)/1 + 1 = 16
        self.net.add_module('conv32_64', nn.Conv2d(32, 64, kernel_size=5, padding=2))
        
        self.net.add_module('relu_2', nn.ReLU())
        
        # вход 16x16, выход 16x16 (16-5+2x2)/1 + 1 = 16
        self.net.add_module('conv64_128', nn.Conv2d(64, 128, kernel_size=5, padding=2))
        
        self.net.add_module('bn_2', nn.BatchNorm2d(128))    #
        self.net.add_module('do_2', nn.Dropout(0.5))        #
        self.net.add_module('relu_3', nn.ReLU())
        
        # конволюция, которая уменьшает размер картинки в два раза 
        # (16-2)/2+1=8
        self.net.add_module('convhalf_2', nn.Conv2d(128, 128, kernel_size=2, stride=2)) 
        
        # вход 8x8, выход 8x8 (8-3+2x1)/1 + 1 = 8
        self.net.add_module('conv128_512', nn.Conv2d(128, 512, kernel_size=3, padding=1))
        
        # добавляем дропаут и бн
        self.net.add_module('bn_3', nn.BatchNorm2d(512))
        self.net.add_module('do_3', nn.Dropout(0.5))
            
        self.net.add_module('relu_4', nn.ReLU())
                         
        self.fc1 = nn.Linear(512 * 8 * 8, 100)
        self.btc = nn.BatchNorm2d(100)
        self.do  = nn.Dropout(0.5) 
        self.fc2 = nn.Linear(100, 10)

    def forward(self, x):
        x = self.net(x)
        # Вытягиваем многомерный тензор катринки в одну линию
        x = x.view(-1, 512 * 8 * 8)
        x = self.fc1(x)
        x = self.btc(x)
        x = self.do(x)
        x = F.relu(x)
        x = self.fc2(x)
        return x
    
